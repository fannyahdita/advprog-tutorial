package tutorial.javari;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.scene.AmbientLight;
import org.json.JSONException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import tutorial.javari.animal.Animal;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static tutorial.javari.JavariDatabase.animals;
import static tutorial.javari.JavariDatabase.readCsv;

public class JavariController {
    private final AtomicInteger counter = new AtomicInteger();

    @RequestMapping(method = GET, value = "/javari")
    public String showAnimal() throws JsonProcessingException {
        readCsv();
        if(animals.size() > 0) {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(animals);
        } else return "No animal in the zoo";
    }

    @RequestMapping(method = GET, value = "javari/{id}")
    public String showAnimal(@PathVariable Integer id) throws JsonProcessingException {
        readCsv();

        Optional<Animal> showAnimal = animals.stream()
                .filter(animal -> animal.getId().equals(id)).findFirst();
        if (!showAnimal.isPresent()) {
            return "No animal with id " + id;
        } else {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(showAnimal.get());
        }
    }

    @RequestMapping(method = DELETE, value = "javari/{id}")
    public String deleteAnimal(@PathVariable Integer id) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        Animal delete = JavariDatabase.deleteAnimal(id);
        if (delete != null) return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(delete);
        else return "No Matched animal with id " + id;
    }

    @RequestMapping(method = POST, value = "javari/{json}")
    public String addAnimal(@PathVariable String json) throws JsonProcessingException, JSONException {
        ObjectMapper objectMapper = new ObjectMapper();
        Animal animal = JavariDatabase.addToDb(json);
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(animal);
    }

}