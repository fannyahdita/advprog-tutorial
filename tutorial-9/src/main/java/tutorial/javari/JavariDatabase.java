package tutorial.javari;

import org.json.JSONException;
import org.json.JSONObject;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JavariDatabase {
    public static final List<Animal> animals = new ArrayList<>();

    public static void readCsv() {
        Scanner scanner;
        try {
            scanner = new Scanner(new File("animal_data.csv"));
            scanner.useDelimiter(",");
            String[] line;

            while (scanner.hasNextLine()) {
                line = scanner.nextLine().split(",");
                animals.add(new Animal(Integer.parseInt(line[0]), line[1], line[2],
                        Gender.parseGender(line[3]), Double.parseDouble(line[4]),
                        Double.parseDouble(line[5]), Condition.parseCondition(line[6])));
            }

            scanner.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }

    public static Animal deleteAnimal(int id) {
        readCsv();
        Animal delete = null;

        try {
            Scanner scanner = new Scanner(new FileReader("animal_data.csv"));
            String[] lines;
            String line;
            List<String> newCsv = new ArrayList<>();

            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                lines = line.split(",");
                if(Integer.parseInt(lines[0]) == id) {
                    delete = new Animal(Integer.parseInt(lines[0]), lines[1], lines[2],
                            Gender.parseGender(lines[3]), Double.parseDouble(lines[4]),
                            Double.parseDouble(lines[5]), Condition.parseCondition(lines[6]));
                    animals.remove(delete);
                } else newCsv.add(line);
            }

            scanner.close();
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("animal_data.csv"));
            for (int i = 0; i < newCsv.size(); i++) {
                bufferedWriter.append(newCsv.get(i)).append("\n");
                bufferedWriter.flush();
            }
            bufferedWriter.close();
        } catch (Exception e){
            e.printStackTrace();
        } return delete;
    }
    public  static Animal jsonToAnimal(String input) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        Animal animal = new Animal(jsonObject.getInt("id"), jsonObject.getString("type"),
                jsonObject.getString("name"),
                Gender.parseGender(jsonObject.getString("gender")),
                jsonObject.getDouble("length"),
                jsonObject.getDouble("weight"),
                Condition.parseCondition(jsonObject.getString("condition")));
        animals.add(animal);
        return animal;
    }

    private static String animaltoCsv(Animal animal) {
        String[] tupleAnimal = {animal.getId().toString(), animal.getType(),
                animal.getName(), animal.getGender().toString(),
                String.valueOf(animal.getLength()), String.valueOf(animal.getWeight()),
                animal.getCondition().toString()};
        return String.join(",", tupleAnimal);
    }

    public static Animal addToDb(String json) throws JSONException {
        Animal newAnimal = jsonToAnimal(json);
        animaltoCsv(newAnimal);
        return newAnimal;

    }
}
