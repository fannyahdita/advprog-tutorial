package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter extends TallyCounter {
    private AtomicInteger atomicInteger = new AtomicInteger();

    public void increment() {
        atomicInteger.getAndIncrement();
    }

    public void decrement() {
        atomicInteger.getAndDecrement();
    }

    public int value() {
        return atomicInteger.intValue();
    }
}
