package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

/**
 * Created dita on 3/9/2018.
 */
public class CheddarCheese implements Cheese {

    public String toString() {
        return "Shredded Cheddar";
    }
}
