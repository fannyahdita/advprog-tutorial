package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

/**
 * Created by HP on 3/9/2018.
 */
public class BreadCrumbsDough implements Dough {
    public String toString() {
        return "Bread Crumbs Dough";
    }
}
