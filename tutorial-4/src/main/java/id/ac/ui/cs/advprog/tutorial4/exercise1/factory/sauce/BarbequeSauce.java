package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

/**
 * Created by dita on 3/9/2018.
 */
public class BarbequeSauce implements Sauce {
    public String toString() {
        return "Barbeque Sauce";
    }
}
