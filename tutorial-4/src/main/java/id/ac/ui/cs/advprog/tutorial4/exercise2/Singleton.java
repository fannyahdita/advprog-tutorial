package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
    static Singleton objectSingleton;

    // TODO Implement me!
    // What's missing in this Singleton declaration?

    private Singleton() {
        objectSingleton = null;
    }

    public static Singleton getInstance() {
        // TODO Implement me!
        if (objectSingleton == null) {
            objectSingleton = new Singleton();
        }

        return objectSingleton;
    }
}
