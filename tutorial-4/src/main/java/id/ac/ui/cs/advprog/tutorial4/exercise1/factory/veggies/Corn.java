package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

/**
 * Created by HP on 3/9/2018.
 */
public class Corn implements Veggies {
    public String toString() {
        return "Corn";
    }
}
