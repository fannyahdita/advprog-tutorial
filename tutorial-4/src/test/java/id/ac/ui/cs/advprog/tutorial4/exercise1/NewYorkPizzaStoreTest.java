package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by HP on 3/9/2018.
 */
class NewYorkPizzaStoreTest {
    PizzaStore newYorkPizzaStore;

    @BeforeEach
    void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    void createPizza() {
        Pizza pizza = newYorkPizzaStore.orderPizza("cheese");
        assertNotNull(pizza);

        pizza = newYorkPizzaStore.orderPizza("clam");
        assertNotNull(pizza);

        pizza = newYorkPizzaStore.orderPizza("veggie");
        assertNotNull(pizza);

    }

}