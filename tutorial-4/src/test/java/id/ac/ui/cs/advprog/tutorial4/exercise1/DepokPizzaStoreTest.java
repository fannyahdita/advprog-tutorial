package id.ac.ui.cs.advprog.tutorial4.exercise1;


import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by dita on 3/9/2018.
 */
class DepokPizzaStoreTest {
    PizzaStore depokPizzaStore;

    @BeforeEach
    void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    void createPizza() {
        Pizza pizza = depokPizzaStore.orderPizza("cheese");
        assertNotNull(pizza);

        pizza = depokPizzaStore.orderPizza("clam");
        assertNotNull(pizza);

        pizza = depokPizzaStore.orderPizza("veggie");
        assertNotNull(pizza);

    }
}