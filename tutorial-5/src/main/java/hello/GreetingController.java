package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {
        if (!name.equals("")) {
            model.addAttribute("name", name);

            List<String> cv = new ArrayList<String>();
            cv.add("Name : FANNYAH DITA CAHYA");
            cv.add("Birth : Bogor, 31 October 1998");
            cv.add("Address : Bukit Cimanggu City");
            cv.add("Education:");
            cv.add("- YPS Singkole Primary High School");
            cv.add("- SMPIT Attaufiq");
            cv.add("- SMAN 1 Bogor");
            cv.add("- Universitas Indonesia");
            cv.add("Description: ");
            cv.add("I am confused.");


            model.addAttribute("cv", cv);

        } else {
            model.addAttribute("name", "");
        }

        return "greeting";
    }

}
