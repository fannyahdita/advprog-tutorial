import java.util.Arrays;
import java.util.List;
import org.junit.Test;

import org.junit.Test;

import static org.junit.Assert.*;

public class PrimeCheckerTest {
    private static final List<Integer> PRIME_NUMBERS = Arrays.asList(2, 3, 5, 7);
    private static final List<Integer> NON_PRIME_NUMBERS = Arrays.asList(4, 6, 8, 9);

    @Test
    public void testIsPrimeTrueGivenPrimeNumbers() {
        PRIME_NUMBERS.forEach(number -> assertTrue(PrimeChecker.isPrime(number)));
    }

    @Test
    public void testIsPrimeFalseGivenNonPrimeNumbers() {
        //fail("TODO Implement me!");
        // Given non-prime numbers
        // When isPrime is invoked
        // It should return false
        NON_PRIME_NUMBERS.forEach(number -> assertFalse(PrimeChecker.isPrime(number)));
    }

}