package applicant;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.function.Predicate;

public class ApplicantTest {

    private Predicate<Applicant> creditEvaluator;
    private Predicate<Applicant> criminalRecordsEvaluator;
    private Predicate<Applicant> employmentEvaluator;
    private Predicate<Applicant> qualifiedEvaluator;
    private Applicant applicant;

    @Before
    public void setUp(){
        applicant = new Applicant();
        qualifiedEvaluator  = Applicant::isCredible;
        creditEvaluator = applicant1 -> applicant1.getCreditScore() > 600;
        employmentEvaluator = applicant1 -> applicant1.getEmploymentYears() > 1;
        criminalRecordsEvaluator = applicant1 -> !applicant1.hasCriminalRecord();
    }

    @Test
    public void getCreditScoreTest(){
        Assert.assertEquals(700, applicant.getCreditScore());
    }

    @Test
    public void getEmploymentTest(){
        Assert.assertEquals(10, applicant.getEmploymentYears());
    }

    @Test
    public void hasCriminalRecordTest(){
        Assert.assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void applicant1isQualifiedTest(){
        Assert.assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator));
    }

    public void applicant2isQualifiedTest(){
        Assert.assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator)));
    }

    public void applicant3isQualifiedTest(){
        Assert.assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator).and(criminalRecordsEvaluator)));
    }

    public void applicant4isQualifiedTest(){
        Assert.assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator).and(creditEvaluator).and(criminalRecordsEvaluator)));
    }
}
