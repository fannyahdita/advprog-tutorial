import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!

    public static final Map<String, Integer> SCORES = new HashMap<>();

    @Before
    public void setUp(){
        SCORES.put("Alice", 12);
        SCORES.put("Bob", 15);
        SCORES.put("Charlie", 11);
        SCORES.put("Delta", 15);
        SCORES.put("Emi", 15);
        SCORES.put("Fransisca", 11);
    }

    @Test
    public void groupByScoresTest(){
        Assert.assertEquals(ScoreGrouping.groupByScores(SCORES).toString(),
                "{11=[Fransisca, Charlie], 12=[Alice], 15=[Emi, Bob, Delta]}");
    }


}