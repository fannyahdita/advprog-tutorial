import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {
    private Customer customer;
    private Movie movie, movie1;
    Rental rent, rent1;

    @Before
    public void setUp(){
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie1 = new Movie("Cinderella", Movie.REGULAR);
        rent = new Rental(movie, 3);
        rent1 = new Rental(movie1, 3);
        customer = new Customer("Alice");
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
//        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent1);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(5, lines.length);
//        assertTrue(result.contains("Amount owed is 7.0"));
        assertTrue(result.contains("2 frequent renter points"));
    }

    @Test
    public void htmlStatementTest() {
        customer.addRental(rent);
        customer.addRental(rent1);

        String res = customer.htmlStatement();
        String[] lines = res.split("\n");

        assertEquals(5, lines.length);
    }
}