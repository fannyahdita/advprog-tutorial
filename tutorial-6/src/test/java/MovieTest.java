import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Movie movie, movie1;

    @Before
    public void setUp(){
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie1 = new Movie("Cinderella", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");
        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);
        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equals(){
        assertEquals(movie.equals(movie1), false);
        assertFalse(movie.hashCode() == movie1.hashCode());
        assertFalse(movie.equals(null));
    }
}