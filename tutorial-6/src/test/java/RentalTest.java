import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {

    private Movie movie, movie1, movie2;
    private Rental rent, rent1, rent2;

    @Before
    public void setUp(){
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);

        movie1 = new Movie("Moana", Movie.CHILDREN);
        rent1 = new Rental(movie1, 4);

        movie2 = new Movie("Coco", Movie.NEW_RELEASE);
        rent2 = new Rental(movie2, 2);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void moviePriceTest() {
        assertEquals("" + rent.moviePrice(), "3.0");
        assertEquals("2.0", "" + rent1.moviePrice());
        assertEquals("6.0", "" + rent2.moviePrice());
    }

    @Test
    public void frequentRenterPointTest(){
        assertTrue(rent.frequentRenterPointOf() == 1);

        movie.setPriceCode(Movie.NEW_RELEASE);
        assertTrue(rent.frequentRenterPointOf() == 2);
    }
}