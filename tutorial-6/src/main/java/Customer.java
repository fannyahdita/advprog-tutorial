import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(each.moviePrice()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(movieCharge()) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints())
                + " frequent renter points";

        return result;
    }

    private double movieCharge() {
        double res = 0;
        Iterator<Rental> iterator = rentals.iterator();
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            res += each.moviePrice();
        }
        return res;
    }

    private int frequentRenterPoints() {
        int res = 0;
        Iterator<Rental> iterator = rentals.iterator();
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            res += each.frequentRenterPointOf();
        }
        return  res;
    }

    public String htmlStatement() {
        Iterator<Rental> iterator = rentals.iterator();
        String res = "<h1> Rental Record for <em>" + getName()
                + "</em></h1>\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // rented movie
            res += each.getMovie().getTitle()
                    + ":" + String.valueOf(each.moviePrice())
                    + "<br>\n";
        }

        // lines
        res += "<p>Amount owed is <em>" + String.valueOf(movieCharge())
                + "</em></p>\n";

        res += "<p>You earned <em>" + String.valueOf(frequentRenterPoints())
                + "frequent renter points</em></p>";
        return res;
    }
}