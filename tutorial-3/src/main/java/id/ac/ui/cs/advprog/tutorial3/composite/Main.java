package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

/**
 * Created by dita on 3/2/2018.
 */
public class Main {
    public static void main(String[] args){
        Company company = new Company();

        Ceo ceo = new Ceo("Leon", 200000);
        Cto cto = new Cto("Gibbs", 100000);

        company.addEmployee(ceo);
        company.addEmployee(cto);

        BackendProgrammer backendProgrammer = new BackendProgrammer("McGee", 20000);
        FrontendProgrammer frontendProgrammer = new FrontendProgrammer("Bishop", 30000);
        NetworkExpert networkExpert = new NetworkExpert("Tony", 50000);
        SecurityExpert securityExpert = new SecurityExpert("Kate", 80000);
        UiUxDesigner uiUxDesigner = new UiUxDesigner("Ziva", 900000);

        company.addEmployee(backendProgrammer);
        company.addEmployee(frontendProgrammer);
        company.addEmployee(networkExpert);
        company.addEmployee(securityExpert);
        company.addEmployee(uiUxDesigner);

        for(Employees employees : company.getAllEmployees()){
            System.out.println(employees.getName() + " as the " + employees.getRole() + " with salary " + employees.getSalary());
        }

        System.out.println("Total salary : " + company.getNetSalaries());
    }
}
