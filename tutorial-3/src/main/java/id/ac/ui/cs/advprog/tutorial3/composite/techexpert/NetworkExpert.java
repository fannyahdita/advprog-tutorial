package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

/**
 * Created by dita on 3/1/2018.
 */
public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        this.name = name;
        this.role = "Network Expert";
        if(salary >= 50000.00){
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
