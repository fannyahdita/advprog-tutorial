package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

/**
 * Created by dita on 3/2/2018.
 */
public class Main {
    public static void main(String[] args){
        CrustySandwich crustySandwich = new CrustySandwich();
        NoCrustSandwich noCrustSandwich = new NoCrustSandwich();
        ThickBunBurger thickBunBurger = new ThickBunBurger();
        ThinBunBurger thinBunBurger = new ThinBunBurger();

        BeefMeat beefMeat = new BeefMeat(thickBunBurger);
        TomatoSauce tomatoSauce = new TomatoSauce(thinBunBurger);
        Cheese cheese = new Cheese(crustySandwich);
        ChickenMeat chickenMeat = new ChickenMeat(noCrustSandwich);

        System.out.println(beefMeat.getDescription() + "; " + beefMeat.cost());
        System.out.println(tomatoSauce.getDescription() + "; " + tomatoSauce.cost());
        System.out.println(cheese.getDescription() + "; " + cheese.cost());
        System.out.println(chickenMeat.getDescription() + "; " + chickenMeat.cost());
    }
}
