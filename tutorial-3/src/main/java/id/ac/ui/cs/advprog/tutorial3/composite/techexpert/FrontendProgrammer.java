package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

/**
 * Created by dita on 3/1/2018.
 */
public class FrontendProgrammer extends Employees{
    public FrontendProgrammer(String name, double salary) {
        this.name = name;
        this.role = "Front End Programmer";
        if(salary >= 30000.00){
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary(){
        return salary;
    }
}
