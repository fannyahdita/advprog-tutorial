package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by dita on 3/1/2018.
 */
public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding tomato";
    }

    @Override
    public double cost() {
        return 0.50 + food.cost();
    }
}
