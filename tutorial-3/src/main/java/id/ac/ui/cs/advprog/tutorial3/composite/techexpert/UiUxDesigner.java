package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

/**
 * Created by HP on 3/1/2018.
 */
public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        this.name = name;
        this.role = "UI/UX Designer";
        if(salary >= 90000.00){
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
