package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by HP on 3/1/2018.
 */
public class Cto extends Employees{
    public Cto(String name, double salary) {
        this.name = name;
        this.role = "CTO";
        if(salary >= 100000.0){
            this.salary = salary;
        } else {
            throw new IllegalArgumentException();
        }
    }

        @Override
        public double getSalary() {
            return salary;
        }


}
